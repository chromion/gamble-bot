export interface Command {
  name: string;
  aliases?: string[];
  execute: (msg: any, args: string[]) => {};
}

export interface User {
  username: string;
  points: number;
}
