import dotenv from "dotenv";
import Discord from "discord.js";
import { CronJob } from "cron";
import db from "./db";
import commands from "./commands";
import { Command } from "./types";

// load env vars
dotenv.config();

const job = new CronJob(
  "0 0 8 * * *",
  function () {
    db.query("UPDATE users SET points = points + 1000")
    console.log(`Added 1000 points to all users`);
  },
  null,
  true,
  "Europe/Berlin"
);

const PREFIX = process.env.NODE_ENV === "production" ? "!" : "&";

class Client extends Discord.Client {
  commands: Discord.Collection<string, Command> = new Discord.Collection();
}

const client = new Client();

// init commands
commands.forEach((command: Command) => {
  client.commands.set(command.name, command);
});

client.on("ready", async () => {
  job.start();
  console.log("Next cron execution", job.nextDates());
  await db.init()
  console.log("DB loaded successfully");
  console.log(`Logged in as ${client.user?.tag}!`);
});

client.on("message", async (msg) => {
  // ignore msg that do not start with PREFIX bot messages
  if (!msg.content.startsWith(PREFIX) || msg.author.bot) return;

  // split msg by space to get args
  const args = msg.content.slice(PREFIX.length).trim().split(/ +/);
  // first arg = command
  // shift to remove TOKEN
  const commandName = args.shift()?.toLowerCase() || "";

  const command =
    client.commands.get(commandName) ||
    client.commands.find(
      (cmd) => !!cmd.aliases && cmd.aliases.includes(commandName)
    );

  if (!command) return;

  try {
    command.execute(msg, args);
  } catch (error) {
    console.error(error);
    msg.reply("There was an error trying to execute that command");
  }
});

client.login(process.env.DISCORD_TOKEN);

// const points = db.data[msg.author.id] ? db.data[msg.author.id].points : 0;
// let lastDaily = db.data[msg.author.id]?.lastDaily;
// if (lastDaily) {
//   const now = new Date().getTime();
//   lastDaily = new Date(lastDaily).getTime();
//   const diff = now - lastDaily;
//   // hours = diff / ms / seconds / minutes
//   const hours = Math.floor(diff / 1000 / 60 / 60);
//   if (hours < 24) {
//     return msg.reply(`Your next daily payout is in ${24 - hours} ready!`);
//   }
// }
// db.data[msg.author.id] = {
//   username: msg.author.username,
//   points: points + 1000,
//   lastDaily: new Date(),
// };
// return await db.write();
