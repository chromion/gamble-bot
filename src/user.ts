import db from "./db";

export class User {
  id: string;
  username: string;
  points: number;

  constructor(id: string, username: string, points: number = 0) {
    this.id = id;
    this.username = username;
    this.points = points;
  }

  async save() {
    const {
      rows: [existingUser],
    } = await db.query("SELECT * FROM users WHERE id = $1", [this.id]);
    let user;
    if (existingUser) {
      const res = await db.query(
        "UPDATE users SET id = $1, username = $2, points = $3 WHERE id = $1 RETURNING *",
        [this.id, this.username, this.points.toString()]
      );
      user = res.rows[0];
    } else {
      const res = await db.query(
        "INSERT INTO users(id, username, points) VALUES($1, $2, $3) RETURNING *",
        [this.id, this.username, this.points.toString()]
      );
      user = res.rows[0];
    }
    return user;
  }

  static async findById(id: string): Promise<User | undefined> {
      const {rows: [user]} = await db.query("SELECT * FROM users WHERE id = $1", [id])
      if(!user) {
          return
      }
      return new User(user.id, user.username, parseInt(user.points))
  }

  static async find(): Promise<User[]> {
      const {rows: users} = await db.query("SELECT * FROM users")
      return users.map(user => new User(user.id, user.username, user.points))
  }
}
