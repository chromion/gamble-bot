import { Pool } from "pg";

let pool: Pool;

async function query(text: string, params?: string[]) {
  const res = await pool.query(text, params);
  return res;
}

async function getClient() {
  const client = await pool.connect();
  return client;
}

async function init() {
  pool = new Pool({
    connectionString: process.env.DATABASE_URL,
    ssl: {
      rejectUnauthorized: false
    }
  });
  await query(
    "CREATE TABLE IF NOT EXISTS users (id VARCHAR PRIMARY KEY, username VARCHAR NOT NULL, points BIGINT DEFAULT 0)"
  );
}

export default {
  init,
  query,
  getClient,
};
