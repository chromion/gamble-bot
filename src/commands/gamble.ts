import { User } from "../user";
import { Command } from "../types";
import { getUserFromMention } from "../utils/getUserFromMention";

// join the game
export const join: Command = {
  name: "join",
  execute: async (msg, args) => {
    let user = msg.author;
    if (args.length > 0) {
      user = getUserFromMention(msg.client, args[0]);
      if (!user) {
        return msg.reply("Invalid arguments");
      }
    }
    const newUser = new User(user.id, user.username);
    await newUser.save();
    return msg.reply(
      `${
        user.username === msg.author.username ? "You" : user.username
      } are now a part of the game`
    );
  },
};

// how many points do i have?
export const points: Command = {
  name: "points",
  execute: async (msg, args) => {
    let user = msg.author;
    if (args.length > 0) {
      user = getUserFromMention(msg.client, args[0]);
      if (!user) {
        return msg.channel.send("Invalid arguments");
      }
    }
    const theUser = await User.findById(user.id);
    if (!theUser) {
      return msg.channel.send("Error user does not exist");
    }
    const { points } = theUser;
    // const points = await getDB().getPoints(user.id);
    return msg.channel.send(
      `${
        user.username === msg.author.username ? "You" : user.username
      } have ${points} :coin:`
    );
  },
};

// add 1000 points to everyone
export const addPoints: Command = {
  name: "addpoints",
  aliases: ["add-points"],
  execute: async (msg) => {
    const user = await User.findById(msg.author.id);
    if (!user) {
      return msg.channel.send("Error user does not exist");
    }
    user.points += 1000;
    await user.save();
    return msg.channel.send(`Added 1000 :coin: to ${msg.author.username}`);
  },
};

// get your daily payout
export const daily: Command = {
  name: "daily",
  execute: (msg) => {
    return msg.channel.send(
      "If you joined the game you get every day at 8pm 1000 :coin:"
    );
  },
};

// gamble for your life
export const gamble: Command = {
  name: "gamble",
  execute: async (msg, args) => {
    const user = await User.findById(msg.author.id);
    if (!user) {
      return msg.channel.send("Error user does not exists");
    }
    const { points = 0 } = user;
    if (!points) {
      return msg.channel.send(
        "It seems like you do not have any points, get your daily payout by typing `!daily`"
      );
    }

    let bet = parseInt(args[0]);

    let betArg = args[0];
    if (betArg.includes("%")) {
      const bets = betArg.split("%");
      bet = points * (parseFloat(bets[0]) / 100);
      bet = Math.floor(bet)
    }

    if (betArg.includes("k")) {
      const bets = betArg.split("k");
      bet = parseInt(bets[0]) * 1000;
    }

    if (betArg.includes("m")) {
      const bets = betArg.split("k");
      bet = parseInt(bets[0]) * 1000000;
    }

    if (!bet) {
      return msg.channel.send(
        "You did not set how much you want to bet, use `!gamble 101`"
      );
    }
    if (bet > points) {
      bet = points;
    }

    // 75/25 chance to win gamble
    const rand = Math.random();
    console.log(rand);
    if (rand <= 0.75) {
      // win => add points
      user.points = points + bet;
      await user.save();
      msg.channel.send(`Congrats you won ${bet} :coin:`)
      // msg.reply(`Congrats you won ${bet} :coin:`);
    } else {
      // win => subtract points
      user.points = points - bet;
      await user.save();
      msg.channel.send(`Oh no you lost ${bet} :coin:`);
      // msg.reply(`Oh no you lost ${bet} :coin:`);
    }
  },
};

export const top: Command = {
  name: "top",
  execute: async (msg) => {
    const users = await User.find();
    const content: string[] = users
      .sort((a: any, b: any) => b.points - a.points)
      .map((user: any, index) => {
        return `${index + 1}. ${index === 0 ? ":trophy: " : ""}${
          user.username
        } ${user.points} :coin:`;
      });
    if (content.length === 0) {
      return msg.channel.send("No players");
    }
    return msg.channel.send(content);
  },
};

export const givepoints: Command = {
  name: "givepoints",
  aliases: ["gp"],
  execute: async (msg, args) => {
    let user;
    if (args.length > 0) {
      user = getUserFromMention(msg.client, args[0]);
      if (!user) {
        return msg.reply("Invalid arguments");
      }
    }

    if (!user) {
      return msg.reply(`Could not find ${args[0]}`);
    }

    let points: number = parseInt(args[1]);
    if (!points) {
      return msg.reply("You did not provide points");
    }
    const firstUser = await User.findById(msg.author.id);
    if (!firstUser) {
      return msg.reply("Error user does not exist");
    }
    points = points > firstUser.points ? firstUser.points : points;
    firstUser.points -= points;
    await firstUser.save();
    const secondUser = await User.findById(user.id);
    if (!secondUser) {
      return msg.reply("Error user does not exist");
    }
    secondUser.points += points;
    await secondUser.save();
    return msg.channel.send(
      `${msg.author.username} gave ${user.username} ${points} :coin:`
    );
  },
};
