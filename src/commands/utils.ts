import { Command } from "../types";
// import { getDB } from "../db";

export const dev: Command = {
  name: "dev",
  execute: (msg) => {
    return msg.channel.send(
      "Feel free to contribute: https://gitlab.com/chromion/gamble-bot"
    );
  },
};

export const dbCommand: Command = {
  name: "db",
  execute: (msg) => {
    return msg.reply("DB");
    // return msg.reply(db.file);
  },
};
